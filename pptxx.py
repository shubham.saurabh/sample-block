from pptx import Presentation  # for this we have to install it : pip install python-pptx
import os                      #If you want to open the ppt just asfter creation without going to the folder and clicking it.
from wand.image import Image
from pptx.util import Inches, Pt



# resized the image of the logo as it was not looking good.
# we may skip this and change the file name of the logo as "nike_black.png"
with Image(filename='nike_black.png') as img:
    img.resize(800, 400)
    img.save(filename='nike_black.png')



def adding_logo(imagename):
    '''function to add logo to the images'''
    with Image(filename=imagename) as image:
        # Import the watermark image
        with Image(filename='nike_black.png') as water:
            # Clone the image in order to process
            with image.clone() as watermark:
                watermark.watermark(water, 0.5, 0, 30)
                # Save the image
                watermark.save(filename=imagename)

prs = Presentation()
for images in range(1, 6):
    ext = ".jpg"
    file = "image"
    n = str(images)
    loged_image = file + n + ext
    adding_logo(loged_image)
    with Image(filename=loged_image) as img:
        img.resize(340, 260)
        img.save(filename=loged_image)
    layout = prs.slide_layouts[6]
    slide = prs.slides.add_slide(layout)
    k="Heading of slide"
    st="this is heading for slide"
    left = top = width = height = Inches(0.2)
    imgleft = Inches(2)
    imgtop = Inches(2)
    pic = slide.shapes.add_picture(loged_image,
                                   imgleft, imgtop)

    # creating textBox
    txBox = slide.shapes.add_textbox(left, top,
                                     width, height)

    # creating textFrames
    tf = txBox.text_frame

    # adding Paragraphs
    p = tf.add_paragraph()

    # adding text
    p.text =k+" "+n

    # font
    p.font.bold = True
    p.font.italic = True
    p.font.size = Pt(40)

    p = tf.add_paragraph()
    p.text = st+" "+n

    # save file
    prs.save('Myppt1.pptx')


print("done")

os.startfile("Myppt1.pptx")
# from pptx import Presentation  # for this we have to install it : pip install python-pptx
# import os
# from pptx.util import Inches
# from wand.image import Image
#
# # resized the image of the logo as it was not looking good.
# # we may skip this and change the file namr of the logo as "nike_black.png"
# with Image(filename='image3.jpg') as img:
#     img.resize(340, 260)
#     img.save(filename='resized1.jpg')
#
# prs = Presentation()
# for i in range(3):
#     layout = prs.slide_layouts[8]
#     slide = prs.slides.add_slide(layout)
#     k="this is  slide"
#     title=slide.shapes.title.text=k+" "+str(i)
#     title=slide.shapes.title.font.name = 'Calibri'
#     subtitle=slide.placeholders[2].text="this is subtitle"
#     image=slide.placeholders[1].insert_picture("resized1.jpg")
#     prs.save("Myptt1.pptx")
#     os.startfile("Myptt1.pptx")
# img="new1.jpg"
# from_left=Inches(2)
# from_top=Inches(0.7)
# width=Inches(5)
# hright=Inches(4.5)
# add_picture=slide.shapes.add_picture(img,from_left,from_top,width,hright)


#import required things


from pptx import Presentation
from pptx.util import Inches, Pt
import os
# Creating Object
ppt = Presentation()
img_path = 'resized1.jpg'

# To create blank slide layout
# We have to use 6 as an argument
# of slide_layouts
blank_slide_layout = ppt.slide_layouts[6]

# Attaching slide obj to slide
slide = ppt.slides.add_slide(blank_slide_layout)

# For adjusting the Margins in inches
left = top = width = height = Inches(0.2)
imgleft = Inches(2)
imgtop = Inches(2)
pic = slide.shapes.add_picture(img_path,
                               imgleft, imgtop)

# creating textBox
txBox = slide.shapes.add_textbox(left, top,
								width, height)

# creating textFrames
tf = txBox.text_frame

# adding Paragraphs
p = tf.add_paragraph()



# adding text
p.text = "Heading of the slide"

# font
p.font.bold = True
p.font.italic = True
p.font.size = Pt(40)


p = tf.add_paragraph()
p.text = "This is a third paragraph that's big "


# save file
ppt.save('test_2.pptx')

print("done")
os.startfile("test_2.pptx")
